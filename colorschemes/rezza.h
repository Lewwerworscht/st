/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	[0] = "#191919", /* black   */
	[1] = "#803232", /* red     */
	[2] = "#5b762f", /* green   */
	[3] = "#aa9943", /* yellow  */
	[4] = "#324c80", /* blue    */
	[5] = "#706c9a", /* magenta */
	[6] = "#92b19e", /* cyan    */
	[7] = "#ffffff", /* white   */

	/* 8 bright colors */
	[8]  = "#252525", /* black   */
	[9]  = "#982b2b", /* red     */
	[10] = "#89b83f", /* green   */
	[11] = "#efef60", /* yellow  */
	[12] = "#2b4f98", /* blue    */
	[13] = "#826ab1", /* magenta */
	[14] = "#a1cdcd", /* cyan    */
	[15] = "#dddddd", /* white   */

	/* special colors */
	[256] = "#222222", /* background */
	[257] = "#dddddd", /* foreground */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;
static unsigned int defaultrcs = 256;
unsigned int bg = 16, bgUnfocused = 16;

/*
 * Colors used, when the specific fg == defaultfg. So in reverse mode this
 * will reverse too. Another logic would only make the simple feature too
 * complex.
 */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
